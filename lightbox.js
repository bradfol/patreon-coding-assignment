window.LightBox = class LightBox {

    /**
     * @class LightBox
     * @constructor
     * @param {String} id The DOM ID of the root element for the LightBox.
     */
    constructor (id) {
        this.rootElement = document.getElementById(id);
        this.rootElement.className = "hidden"; // Hidden by default

        // If user clicks on background, dismiss the light box
        this.rootElement.addEventListener("click", () => {
            this.dismiss();
        });

        this.imageElement = this.rootElement.getElementsByClassName("hero")[0];

        this.previousArrowElement = this.rootElement.getElementsByClassName("previous")[0];
        this.previousArrowElement.addEventListener("click", event => {
            event.stopPropagation();
            const newIndex = this.currentIndex - 1;
            this._updateImageIndex(newIndex);
        });

        this.nextArrowElement = this.rootElement.getElementsByClassName("next")[0];
        this.nextArrowElement.addEventListener("click", event => {
            event.stopPropagation();
            const newIndex = this.currentIndex + 1;
            this._updateImageIndex(newIndex);
        });

        this.titleElement = this.rootElement.getElementsByClassName("title")[0];
        this.descriptionElemeent = this.rootElement.getElementsByClassName("description")[0];
    }

    /**
     * Call this method with an array of image objects to show the LightBox.
     * 
     * Each image object in the array should have the following properties:
     * url, title, description
     * 
     * @method show
     * @param {Array<Object>} imageObjects 
     */
    show (imageObjects) {
        // Show the light box, remove "hidden" class
        this.rootElement.className = "";

        this.imageObjects = imageObjects;
        this._updateImageIndex(0);
    }

    _updateImageIndex (index) {
        const count = this.imageObjects.length;
        if (index < 0 || index >= count) {
            // Index out of bounds
            return;
        }
        this.currentIndex = index;
        const imageObject = this.imageObjects[index];
        this.imageElement.src = ""; // Clear the existing image
        this.imageElement.src = imageObject.url;
        this.titleElement.textContent = imageObject.title;
        this.descriptionElemeent.textContent = imageObject.description;

        // Show or hide the arrows based on index
        const previousArrowClass = this.currentIndex <= 0 ? "previous hidden" : "previous";
        this.previousArrowElement.className = previousArrowClass;
        const nextArrowClass = this.currentIndex >= (count - 1) ? "next hidden" : "next";
        this.nextArrowElement.className = nextArrowClass;
    }

    /**
     * Call to hide the light box.
     * @method dismiss
     */
    dismiss() {
        // Hide the light box
        this.rootElement.className = "hidden";
    }

};
