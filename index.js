// Make a request to the Giphy API using the provided search term
function requestGiphySearch (searchTerm, callback) {
    const giphyAPIKey = "87908b9d68ea4affaa2eeb6ca675d7fb";
    const httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = () => {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                callback(null, httpRequest.response);
            }
            else {
                callback(new Error("Invalid response code "+ httpRequest.status));
            }
        }
    };
    httpRequest.responseType = "json";
    const url = "https://api.giphy.com/v1/gifs/search?api_key="+ giphyAPIKey+ "&q="+ encodeURIComponent(searchTerm);
    httpRequest.open("GET", url, true);
    httpRequest.send();
}

// Transform the response from Giphy API into the image objects expected by LightBox
function mapGiphyResponse (response) {
    return response.data.map(element => {
        return {
            url: element.images.original.url,
            title: element.slug,
            description: "Filename: "+ element.id
        };
    });
}

document.addEventListener("DOMContentLoaded", () => {
    // Create lightbox
    const lightbox = new window.LightBox("lightbox");

    function performGiphySearch() {
        const searchTerm = document.getElementById("searchInput").value;
        requestGiphySearch(searchTerm, (error, response) => {
            if (error) {
                alert("Error: "+ error.message);
                return;
            }

            const result = mapGiphyResponse(response);

            // Check if there are results, array length is greater than 0
            if (result.length === 0) {
                alert("No results. Please try a different search.");
                return;
            }

            lightbox.show(result);
        }); 
    }

    // Setup the click listener on the search button
    document.getElementById("searchButton").addEventListener("click", performGiphySearch);
    // Also setup the enter key listener on the search input
    document.getElementById("searchInput").addEventListener("keyup", event => {
        if (event.keyCode === 13) {
            performGiphySearch();
        }
    });
});
